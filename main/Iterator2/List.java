package Iterator2;

public interface List<E>
{
    Iterator<E> iterator();
}
