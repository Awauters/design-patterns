package Adapter;

public class AdapterPatternDemoApp {
    public static void main(String[] args){
        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("mp3", "reinventing hatred.mp3");
        audioPlayer.play("mp4", "C.A.N.C.E.R.mp4");
        audioPlayer.play("vlc", "year of the snake");
        audioPlayer.play("avi", "betray and degrade");
    }
}
