package Adapter2;

public interface ToyDuck {
    //target interface
    //toyducks don't fly they just make squeaking sound
    public void squeak();
}
