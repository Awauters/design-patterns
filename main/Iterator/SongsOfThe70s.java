package Iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class SongsOfThe70s implements SongIterator {

    // ArrayList holds SongInfo objects
    ArrayList<SongInfo> bestSongs;

    public SongsOfThe70s(){

        bestSongs = new ArrayList<SongInfo>();

        addSong("Imagine", "John Lennon", 1971);
        addSong("American Pie", "Don Mclean", 1971);
        addSong("I will survive", "Gloria Gaynor", 1979);
    }
    // add a SongInfo object to the end of the ArrayList

    public void addSong(String songName, String bandName, int yearReleased){
        SongInfo songInfo = new SongInfo(songName, bandName, yearReleased);
        bestSongs.add(songInfo);

    }

    //Return the ArrayList filled with SongInfo Objects

    public ArrayList<SongInfo> getBestSongs() {
        return bestSongs;
    }
    //OLD way
    //public Iterator createIterator(){
    // return (Iterator) bestSongs;

    //NEW by adding this method i'll be be able to treat all collections the same
    public Iterator createIterator(){
        return bestSongs.iterator();
    }
}