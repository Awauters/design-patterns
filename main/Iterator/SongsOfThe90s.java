package Iterator;

import java.util.Hashtable;
import java.util.Iterator;

public class SongsOfThe90s implements SongIterator{

    //Create hashtable with an int as a key and songinfo objects
    Hashtable<Integer, SongInfo> bestSongs = new Hashtable<Integer, SongInfo>();

    //Will increment Hashtable key

    int hashKey = 0;

    public SongsOfThe90s() {
        addSong("Losing My Religion", "REM", 1991);
        addSong("Creep", "Radiohead", 1993);
        addSong("Walk on the Ocean", "Toad the Wet Sprocket", 1991);

    }
    // Add a new songinfo object to the Hashtable and then increment the Hashtable key
    public void addSong(String songName, String bandName, int yearReleased){
        SongInfo songInfo = new SongInfo(songName, bandName, yearReleased);
        bestSongs.put(hashKey, songInfo);

        hashKey++;

    }
    // This is replaced by the iterator
    // return a Hashtable full of songinfo objects
    public Hashtable<Integer, SongInfo> getBestSongs(){
        return bestSongs;
    }
// by adding this method i'll be able to treat all collections the same
    public Iterator createIterator(){
        return bestSongs.values().iterator();
    }
}

